#include "LexAnalizer.h"


LexAnalizer::LexAnalizer(IOunit *unit)
{
	io = unit;
	symbol = 0;

	// ���������� ������ ������!
	// keyTree["key"] = SEMICOLON;
}

LexAnalizer::~LexAnalizer()
{
}

unsigned LexAnalizer::nextSym()
{
	switch (getType())
	{
	case CHARACTER: idKeywordScan(); break;
	case NUMBER: numConstScan(); break;
	case SEMICOLON: symbol = 5; /*SEMICOLON*/ io->nextCh();  break;
	case DOUDOTS: 
		if (io->nextCh() == '=') {
			symbol = 5 /*ASSIGN*/;
			io->nextCh();
		}
		else
			symbol = 6 /*COLON*/;
		break;
	case QUOTE: strConstScan(); break;
	case ELSE: io->Error(6); break;
	default: io->Error(404); break;
	}
	return symbol;
}

unsigned LexAnalizer::curSym()
{
	return symbol;
}

chType LexAnalizer::getType() {
	char ch = io->curCh();
	if (ch >= 'A' && ch <= 'z' || ch == '_')
		return CHARACTER;
	if (ch >= '0' && ch <= '9')
		return NUMBER;
	if (ch == ';')
		return SEMICOLON;
	if (ch == ':')
		return DOUDOTS;
	if (ch == '\'')
		return QUOTE;
	return ELSE;
}

void LexAnalizer::idKeywordScan() {
	unsigned namelenght = 0;
	string name = "";
	chType curChType = getType();
	while (curChType == CHARACTER || curChType == NUMBER){
		name += io->curCh();
		namelenght++;
		curChType = getType();
		io->nextCh();
	}
	if (keyTree.count(name) == 1) {
		symbol = keyTree.at(name);
	}
	else {
		symbol = 1000 /*IDENT*/;
		// TODO : Add to idet TAble
	}
}

void LexAnalizer::numConstScan() {
	int nmb_int = 0;
	double nmb_float = 0;
	while (getType() == NUMBER)
	{
		if (nmb_int < MAX_INT / 10 || nmb_int == MAX_INT / 10 && (io->curCh() - '0' < MAX_INT % 10)) {
			nmb_int = nmb_int * 10 + io->curCh() - '0';
		}
		else {
			// ��������� ������ �� ������ �������� ��� ��� 
		}
		nmb_float = nmb_float * 10 + io->curCh() - '0';
		io->nextCh();
	}
	if (io->curCh() == '.') {
		io->nextCh();
		float deg = 10;
		while (getType() == NUMBER) {
			nmb_float += (io->curCh() - '0') / deg;
			deg *= 10;
			io->nextCh();
		}
		symbol = 51; /*FLOAT_CONST*/
	}
	else
		symbol = 50; /*INT_CONST*/
}

void LexAnalizer::strConstScan()
{
	// TODO

	symbol = 10; /*STR_CONST*/
}
