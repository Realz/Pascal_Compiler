#include "IOunit.h"

IOunit::IOunit()
{
	inputFile.open("D:\input.pas.txt", ifstream::in);
	if (inputFile)
		cout << "Input file successfully opened" << endl;
	else
		cout << "Error occured while opening input file" << endl;
}

IOunit::~IOunit()
{
	inputFile.close();
}

char IOunit::nextCh() {
	if (inputFile >> ch && skipComments())
		return ch;
	else
		return NULL;
	
}

char IOunit::curCh() {
	return ch;
}

void IOunit::Error(unsigned code)
{
	cout << "error code " << code << endl;
}

bool IOunit::skipComments() {
	if (ch == '{') {
		while (inputFile >> ch && ch != '}');
		inputFile >> ch;
	}
	return inputFile.good();
}

int IOunit::pos() {
	return inputFile.tellg();
}
