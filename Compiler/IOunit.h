#pragma once
#include <fstream>
#include <iostream>

using namespace std;

class IOunit
{
public:
	IOunit();
	~IOunit();
	char nextCh();
	char curCh();
	void Error(unsigned code);
	int pos();
private:
	char ch;
	unsigned line;
	unsigned numCh;
	ifstream inputFile;
	bool skipComments();

};

