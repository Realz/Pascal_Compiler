#pragma once
#include <unordered_map>
#include <string>
#include "IOunit.h"

const unsigned MAX_INT = 32564;
enum chType { CHARACTER, NUMBER, SEMICOLON, DOUDOTS, QUOTE, ELSE };

class LexAnalizer
{	
public:		
	LexAnalizer(IOunit*);
	~LexAnalizer();
	unsigned nextSym();
	unsigned curSym();
private:
	IOunit *io;
	unsigned symbol;
	unordered_map <string, unsigned> keyTree;
	chType getType();
	void idKeywordScan();
	void numConstScan();
	void strConstScan();
};

